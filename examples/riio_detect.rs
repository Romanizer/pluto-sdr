// Simple Rust IIO example to list the devices found in the specified context.
//
// Note that, if no context is requested at the command line, this will create
// a network context if the IIOD_REMOTE environment variable is set, otherwise
// it will create a local context. See Context::new().
//
// Copyright (c) 2018-2019, Frank Pagliughi
//
// Licensed under the MIT license:
//   <LICENSE or http://opensource.org/licenses/MIT>
// This file may not be copied, modified, or distributed except according
// to those terms.

// Edited by Roman Hayn, 2023
// -> Added Attribute display and default device URI
// -> Updated clap crate

use clap::Arg;
use industrial_io as iio;
use std::process;

// defaults to my pluto sdr device, if no arguments provided
const DEFAULT_URI: &str = "ip:192.168.2.2";

fn main() {
    let matches = clap::Command::new("riio_detect")
        .about("Rust IIO free scan buffered reads.")
        .arg(
            Arg::new("network")
                .short('n')
                .long("network")
                .help("Use the network backend with the provided hostname"),
        )
        .arg(
            Arg::new("uri")
                .short('u')
                .long("uri")
                .help("Use the context with the provided URI"),
        )
        .get_matches();

    let ctx = if let Some(hostname) = matches.get_one::<String>("network") {
        iio::Context::with_backend(iio::Backend::Network(hostname))
    } else if let Some(uri) = matches.get_one::<String>("uri") {
        iio::Context::from_uri(uri)
    } else {
        iio::Context::from_uri(DEFAULT_URI)
    }
    .unwrap_or_else(|_err| {
        println!("Couldn't open IIO context.");
        process::exit(1);
    });

    let mut trigs = Vec::new();

    if ctx.num_devices() == 0 {
        println!("No devices in the default IIO context");
    } else {
        println!("IIO Devices:");
        for dev in ctx.devices() {
            if dev.is_trigger() {
                if let Some(id) = dev.id() {
                    trigs.push(id);
                }
            } else {
                let num_channels = dev.num_channels();
                print!("{} ", dev.id().unwrap_or_default());
                print!("[{}]", dev.name().unwrap_or_default());
                println!(": {} channel(s)", num_channels);

                // sort attribute list
                let attrs_raw = dev.attr_read_all().unwrap();
                let mut attrs: Vec<_> = attrs_raw.iter().collect();
                attrs.sort_by(|a, b| a.0.cmp(b.0));

                for attr in attrs {
                    println!("  -> {:?}", attr);
                }

                for channel in 0..num_channels {
                    let channel = dev.get_channel(channel).unwrap();
                    let is_output = match channel.is_output() {
                        true => "Output",
                        false => "Input",
                    };
                    print!("  CH: {:?}  ", channel.id().unwrap_or_default());
                    print!("[{}]: ", channel.name().unwrap_or_default());
                    print!("{} ", is_output);
                    println!("{:?}", channel.channel_type());

                    // sort attribute list
                    let ch_attrs_raw = channel.attr_read_all().unwrap();
                    let mut attrs_ch: Vec<_> = ch_attrs_raw.iter().collect();
                    attrs_ch.sort_by(|a, b| a.0.cmp(b.0));

                    for attr in attrs_ch {
                        println!("  -> {:?}", attr);
                    }
                }
            }
        }

        if !trigs.is_empty() {
            println!("\nTriggers Devices:");
            for s in trigs {
                println!("  {}", s);
            }
        } else {
            println!("\nNo Trigger Devices found!");
        }
    }
}
