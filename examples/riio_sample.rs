// Sampling RX_I and RX_Q
use clap::Arg;
use num::Complex;
use pluto_sdr::pluto;
use std::{
    process,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};

// change this to your pluto's URI
// run `iio_info -s` to list connected IIO devices
const DEFAULT_URI: &str = "ip:192.168.2.2";

fn main() {
    let matches = clap::Command::new("riio_sample")
        .about("Rust IIO free scan buffered reads.")
        .arg(
            Arg::new("network")
                .short('n')
                .long("network")
                .help("Use the network backend with the provided hostname"),
        )
        .arg(
            Arg::new("uri")
                .short('u')
                .long("uri")
                .help("Use the context with the provided URI"),
        )
        .get_matches();

    let pluto = if let Some(hostname) = matches.get_one::<String>("network") {
        pluto::Pluto::connect(hostname)
    } else if let Some(uri) = matches.get_one::<String>("uri") {
        pluto::Pluto::connect(uri)
    } else {
        pluto::Pluto::connect(DEFAULT_URI)
    }
    .unwrap_or_else(|| {
        println!("Couldn't open IIO context.");
        process::exit(1);
    });

    // setup LO and Sampling Frequency
    let _ = pluto.set_lo_rx(2400000000);
    let _ = pluto.set_sampling_freq(5000000);

    // setup sampler
    let (rx0_i, rx0_q) = pluto.rx_ch0();

    // Enable I and Q sample channels of ADC
    rx0_i.enable();
    rx0_q.enable();

    let mut buf = pluto.create_buffer_rx(4096).unwrap();

    // <C-c> lsitener
    let quit = Arc::new(AtomicBool::new(false));
    let q = quit.clone();
    ctrlc::set_handler(move || {
        q.store(true, Ordering::SeqCst);
    })
    .expect("Error setting Ctrl-C handler");

    while !quit.load(Ordering::SeqCst) {
        // refill the buffer / fetch samples from pluto
        // blocks until buffer is full
        buf.refill().unwrap();

        let rx0_i_samples = rx0_i.read::<i16>(&buf).unwrap();
        let rx0_q_samples = rx0_q.read::<i16>(&buf).unwrap();

        // combine I and Q samples into Complex numbers: I + j*Q
        let cmplx: Vec<Complex<f32>> = rx0_i_samples
            .iter()
            .zip(rx0_q_samples.iter())
            .map(|(i, q)| Complex::new(i.to_owned() as f32, q.to_owned() as f32))
            .collect();

        println!("{:?}", cmplx);

        println!("sample-size: {}", cmplx.len());
    }

    // disable the I and Q channels
    rx0_i.disable();
    rx0_q.disable();

    // rust will automatically drop(pluto); which disconnects it safely
}
