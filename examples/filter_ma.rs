use pluto_sdr::filter::Filter;

fn main() {
    let signal = vec![1., 4., -3., 5., 10., -19., -4., 10., -2., -1.];
    // coeff describe a 4 point moving average
    let lpf_coefficients = vec![0.25, 0.25, 0.25, 0.25];
    // create the low pass filter
    let lpf = Filter::new(lpf_coefficients.into());

    // filter the signal
    let filtered = lpf.filter_windowed(&signal);
    println!("{:?}", filtered);
}
