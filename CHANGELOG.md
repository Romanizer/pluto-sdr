# pluto-sdr Changelog

## `v0.1.1` Signal Processing
A lot of documentation.

### Breaking
* `Pluto::connect(uri)` now returns an `Option<Pluto>`
* Rename `Pluto::rx_i/q() / tx_i/q()` to `rx_i/q_ch0() / tx_i/q_ch0()`

### Added
* `Pluto::tx_ch0()` and `rx_ch0()` both return the tuple: `(rx_i_ch0, rx_q_ch0)`
* add `filter` module: contains useful functions to apply FIR Filters to signals
* example `riio_sample` simply samples the ADC
* add `pam` module: adds string->pam and pam->string capability (Pulse Amplitude Modulation)
* added Moving Average and SRRC pulse Filter to module `filter`
* `filter` now has method `len()`

### Fixed
* `signal::mean(vec<i16>)` now less likely to overflow. It sums into i64 to prevent overflow.

## `v0.1.0` Initial Release [24.10.23]
* connect to ADALM Pluto SDR via usb/network, read and write samples, and edit basic settings
* basic functions for signal analysis: mean, variance and standard deviation
* `riio_detect` example prints all Devices, Channels and Attributes of Pluto
