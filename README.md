# unofficial libiio abstraction layer for ADALM-Pluto SDR

A high-level abstraction layer for interacting with the [ADALM-Pluto](https://www.analog.com/en/design-center/evaluation-hardware-and-software/evaluation-boards-kits/adalm-pluto.html) SDR.
Inspired by the official Python Library: [pysdr](https://pysdr.org/).

## Getting started

Add this library to your project:
```sh
cargo add pluto-sdr
```

Or edit your `Cargo.toml`:
```
pluto-sdr = "0.1"
```

## Goals

* simple configuration of the Pluto SDR in Rust
* provide useful functions for signal analysis
* provide filtering and convolution options

